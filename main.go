package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"tunnelproxy/handle"
	"tunnelproxy/middleware"

	"github.com/go-pkgz/routegroup"
)

const timeout = 5 * time.Second

func main() {
	// Router
	router := routegroup.New(http.NewServeMux())
	router.Route(func(base *routegroup.Bundle) {
		base.Use(middleware.Log)
		base.HandleFunc("GET /{$}", handle.TunnelProxy)
	})

	// Spice Proxy
	proxy := http.Server{Addr: ":3128", Handler: http.HandlerFunc(handle.TunnelProxy)}
	go func() { log.Println(proxy.ListenAndServe()) }()
	log.Printf("Proxy Running: http://127.0.0.1:3128")

	// Graceful shutdown
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	start := time.Now()
	log.Println("Gracefully shutting down...")
	proxy.Shutdown(ctx)
	log.Printf("Gracefully closed %s", time.Since(start))
}
