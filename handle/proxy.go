package handle

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"time"
)

func TunnelProxy(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodConnect {
		handleTunnel(w, r)
	} else {
		handleHTTP(w, r)
	}
}

func handleTunnel(w http.ResponseWriter, r *http.Request) {
	// print request information
	fmt.Println(r.Method, r.Host, r.URL, r.Proto, r.RemoteAddr)

	dest, err := net.DialTimeout("tcp", r.Host, 10*time.Second)
	if err != nil {
		log.Println(err)
	}
	w.WriteHeader(http.StatusOK)
	hijacker, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "Hijacking not supported", http.StatusInternalServerError)
		return
	}
	src, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	go transfer(dest, src)
	go transfer(src, dest)
}

func transfer(dest io.WriteCloser, src io.ReadCloser) {
	defer dest.Close()
	defer src.Close()
	if _, err := io.Copy(dest, src); err != nil {
		log.Println(err)
	}
}

func handleHTTP(w http.ResponseWriter, r *http.Request) {
	resp, err := http.DefaultTransport.RoundTrip(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	defer resp.Body.Close()
	for k, vv := range resp.Header {
		for _, v := range vv {
			w.Header().Add(k, v)
		}
	}
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}
